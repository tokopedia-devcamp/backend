# Generated by Django 4.1.2 on 2022-10-11 17:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('craftsman', '0003_rename_crafstmanservice_craftsmanservice'),
    ]

    operations = [
        migrations.AddField(
            model_name='craftsman',
            name='category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='craftsman.craftsmancategory'),
        ),
    ]
